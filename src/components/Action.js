import React from 'react';
import './../styles/style.css'
import {withRouter} from "react-router-dom";
class Action extends React.Component{
    addContact = ()=>{
        this.props.history.push('create')
    }
    render(){
        return(
            <div>
                <button onClick={this.addContact} className="action-btn">{this.props.title} </button>
            </div>
        )
    }
}

export default withRouter(Action);