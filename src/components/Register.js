
import React from 'react';
import Action from './Action.js';
import { withRouter } from "react-router-dom";
class Register extends React.Component {

    apiAddress = "https://code-catalist-phone-book-rails.herokuapp.com";

    state = {
        fields: {
            name: '',
            surname: '',
            phone_number: ''
        },
        people: [],

    };
    // getting the change in the input
    onInputChange = (event) => {
        const fields = this.state.fields;
        fields[event.target.name] = event.target.value;
        this.setState({ fields });
    };

    componentWillMount = () => {
        console.log(this.props);
        if (this.props.isEdit) {
            fetch(`${this.apiAddress}/contacts/${this.props.match.params.id}`, {
                method: "get",
                headers: {
                    'Accept': 'application/json, text/plain, */*',
                    'Content-Type': 'application/json'
                },
            })
                .then(resp => {
                    return resp.json()
                })
                .then(resp => {
                    const people = [
                        ...this.state.people,
                        this.state.fields,
                    ];
                    console.log("Should set initial values")
                    this.setState({
                        people,
                        fields: {
                            name: resp.name.split(" ")[0],
                            surname: resp.name.split(" ")[1],
                            phone_number: resp.phone_number
                        }
                    });
                }).catch(err => {
                    console.log("Error editing the contact")
                })
        }
    }
    // submitting form
    onFormSubmit = (event) => {
        // Edit
        let data = {
            name: this.state.fields.name + " " + this.state.fields.surname,
            phone_number: this.state.fields.phone_number
        }
        if (this.props.isEdit === true) {
            fetch(`${this.apiAddress}/contacts/${this.props.match.params.id}`, {
                method: "put",
                headers: {
                    'Accept': 'application/json, text/plain, */*',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            })
                .then(resp => {
                    const people = [
                        ...this.state.people,
                        this.state.fields,
                    ];
                    this.setState({
                        people,
                        fields: {
                            name: '',
                            surname: '',
                            phone_number: ''
                        }
                    });
                    this.props.history.push("/")

                }).catch(err => {
                    console.log("Error editing the contact")
                })
        } else {
            // create
            // 
            fetch(`${this.apiAddress}/contacts`, {
                method: "post",
                headers: {
                    'Accept': 'application/json, text/plain, */*',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            })
                .then(resp => {
                    const people = [
                        ...this.state.people,
                        this.state.fields,
                    ];
                    this.setState({
                        people,
                        fields: {
                            name: '',
                            surname: '',
                            phone_number: ''
                        }
                    });
                    this.props.history.push("/")

                }).catch(err => {
                    console.log("Error saving the contact")
                })
        }

        event.preventDefault();
    }
    
    // go back to the homepage
    goBack = () => {
        this.props.history.push('');
    }
    render() {
        return (
            <div className="RegisterPage">
                <div>
                    <button onClick={this.goBack} className="goBack">
                        <span className="mdi mdi-arrow-left"></span>
                    </button>
                </div>
                <form onSubmit={this.onFormSubmit}>
                    <div className="image-aside">
                        <div>
                            <img src="https://via.placeholder.com/140x100" />
                        </div>
                        <div>
                            <div className="input-h">
                                <label>Name</label>
                                <input placeholder='Name' name='name' value={this.state.fields.name} onChange={this.onInputChange} />
                            </div>

                            <div className="input-h">
                                <label>Surname</label>
                                <input placeholder='Surname' name='surname' value={this.state.fields.surname} onChange={this.onInputChange} />
                            </div>
                        </div>
                    </div>
                    <div className="input-h">
                        <label>Mobile</label>
                        <input type='number' placeholder='Mobile' name='phone_number' value={this.state.fields.phone_number} onChange={this.onInputChange} />
                    </div>
                    <div>
                        <button type="submit" className="action-btn">Savee Contact </button>
                    </div>

                </form>

            </div>
        )
    }
}

export default withRouter(Register);