import React from "react";
import Header from "./../common/Header";

// footer component
class HomePage extends React.Component {
  render() {
    return (
      <div>
        <div class="row">
          <div
            class="col-sm-6 col-md-7 col-lg-7 col-xl-7"
            style={{ border: "1px solid #333" }}
          >
            <div class="card">
              <div class="card-img">
                <img
                  class="card-img-left"
                  src="https://source.unsplash.com/random/301x200"
                  alt=""
                />
              </div>

              <div class="card-body">
                <h4 class="card-title">Card title</h4>
                <p class="card-text">
                  This card has supporting text below as a natural lead-in to
                  additional content.
                </p>
                <p class="card-text">
                  <small class="text-muted">Last updated 3 mins ago</small>
                </p>
              </div>
            </div>
          </div>
          <div
            class="col-sm-6 col-md-5 col-lg-5 col-xl-5"
            style={{ border: "1px solid #333" }}
          >
            <ul>
              <li>Wassup</li>
              <li>Man</li>
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

export default HomePage;
