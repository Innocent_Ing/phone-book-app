import React from 'react';
import Action from './Action';
import {withRouter} from "react-router-dom";

class Phonebook extends React.Component {

    apiAddress = "https://code-catalist-phone-book-rails.herokuapp.com";
    contacts = [];


    visitContact = (id) => {
        this.props.history.push(`/view/${id}`)
    }
    fetchContacts = () => {
        return fetch(`${this.apiAddress}/contacts`)
    }
    
    // execute when the component rendering is complete
    componentDidMount() {
        this.fetchContacts()
            .then(response => {
                return response.json()
            }).then(response => {
                if (response instanceof Array) {
                    this.contacts = response;
                    this.setState({ contacts: response });
                }
            });
    }
    editContact = (id)=>{
        this.props.history.push(`/edit/${id}`)
    }

    // delete contact
    deleteContact = index => {
        let contact =  this.contacts[index];
        fetch(`${this.apiAddress}/contacts/${contact.id}`, {
            method: "Delete"
        }).then(resp => {
            this.contacts = this.state.contacts;
            this.contacts.splice(index, 1);
            this.setState({ contacts: this.contacts });
        }).catch(err => {
            console.log("Failed to delete");
        })
    }
    render() {
        return (
            // phone book manager UI
            <div className="phonebook-manager">
                <h2>Phone Book Manager</h2>
                <div>

                    <table className="numbers">

                        <thead>
                            <tr className="widget-header">
                                <th>Name</th>
                                <th>Phone Number</th>
                                <th>Edit</th>
                                <th>Delete</th>
                            </tr>
                        </thead>

                        <tbody>
                            {
                                this.contacts.map((contact, index) => {
                                    return (
                                        <tr key={index}>
                                            <td onClick={this.visitContact.bind(this, contact.id)}>
                                                <span>{contact.name}</span>
                                            </td>
                                            <td onClick={this.visitContact.bind(this, contact.id)}>{contact.phone_number}</td>
                                            <td>
                                                <button  className="mdi mdi-account-edit" onClick={this.editContact.bind(this, contact.id)}></button>
                                            </td>
                                            <td>
                                                <button className="mdi mdi-delete" onClick={this.deleteContact.bind(this, index)}></button>
                                            </td>

                                        </tr>
                                    )
                                })
                            }
                        </tbody>

                    </table>
                </div>

                <Action title="Add contact" />

            </div>
        )
    }
}

export default withRouter(Phonebook);