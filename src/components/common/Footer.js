import React from "react";

class Footer extends React.Component {
  render() {
    return (
      <div className="footer">
        <div className="logo"></div>
        <div className="profile">
          <p> &copy; All rights are reserved.</p>
        </div>
      </div>
    );
  }
}

export default Footer;
