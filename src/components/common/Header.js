import React from "react";

// footer component
class Header extends React.Component {
  render() {
    return (
      <div className="header">
        <div className="logo">
          <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAX0AAACECAMAAABLTQsGAAAAh1BMVEUfHx////8AAAAYGBgcHBwVFRVGRkYTExMMDAzr6+uKiopfX18hISEZGRmmpqbu7u75+flycnJbW1vMzMy/v78yMjJlZWXz8/NSUlLl5eUnJydLS0tra2sICAje3t7FxcWXl5eysrKFhYV7e3vV1dW0tLQ8PDw/Pz8tLS01NTWenp6UlJR3d3coh9TqAAAMqUlEQVR4nO2caWOCuhKGWYKhKoqCCygoitbt//++y57MAJ56hLand95vxRTCQzLJLKAoJBKJRCKRSCQSiUQikUgkEolEIpFIJBKJRCKRSCQSiUQikUgkEolEIpFIJBKJ9LvEEqEjXEvEWXN7UgdiTDc457rFFP8KQHN/5nqe+xjzn+pbT7IMLP35CNNr/2B10AszCKzLeTPfHoe3UzxVV5r0qzHy1Eze0vpbw389wppfnt2hX/uH5fpt/OP5YT85uU44VQt9SPT5rDq8ehjvXus3ST9OP7D2Wnt7Y1Rr7q31d3thbT9UKIm+PojFcW/z9sV+kZh/UrHiz/bBzCe15tsOjDHfrtrp7+Ufbn/K9OvnWMUatt4hG9uorT0MuuiGtp2C0wr67AI66Bl/yvKbcwxUdRZtd2gecdvJuBsafAaMj0T/CqcF3oz+x6UdaoN/1Db4tRC1dP0OdjypmLmXR4Ggb13hFf2/RZ/VbbnTsu7yJZonO60zFnwp2x5p7C/givy3LE9q+h2Mf9C8s+BoifYWHY38REYb/TG4qPOnVt1UfIl2HKrbOPj1DTI8UYfjsI2+YoC15vDn6CvaDtH/WDd5NeYdNZt8B33mu+L47qkr+N+U+UBY1ahhiLEFtlDfQl/RL9Xg2J3/lK+by5hj+mGDA2uMcKvvoa/oyiQzeeHk8gfhN9Bv8ri02t7om+gn27LN7HCYrXl3i/wvUgP9sOZxsSDxbL0foZ/s+nkae+7uar9JKX3s8R5N1ChIvLLTT9H/y0rpxyjMGKL7Z0acPJKPL9N/9cH8IP0f3kal9Hd4O7mElj91CsLzP9NnOg/SNKCpZcnA9lyNZaTtsrZpo2b63ETKrY+FDxcTldWO55fX29rzsqsB9KGZoRWdM3u3dyl9x0cuVwz7w5Ndd6RN/4E+4/5meXfyZnY8OW785mysxc+jWx699G7rsd5Mnx2HSKP0bGyDDx/GWfMrPj4cpO3Ho1p7M+vsZZnvplTnPlfEfooZi1H+g707bqye1/qc/g3Sn87kwa+vQ3U1/yf6fLw9ZdMjPt2ijO30dPAbfAfzvJcCx3Y0MBrjPPoQL0eT9GTs00WH1TzUatWOP9L21sjDx7U0jzyTmtuTTdlPix0k9/Pj1vM+N6N/WUO0aiQbRD5MkAbP6bNgW4z66DOZs+f83mxnjWNxzJyhrEK40RrHPjsg/Bn9xAFDmO2C/gUni/IhxOfo3mwtOfcEzva4CO1ytoMX3Q16DW9k9Bcsgj2czqW56Cd+7sx8Sl83y9mz01NjyT+LsNBqD4PyrO45qNPFqMnuM47i3zl9RfdhcKSgr1g+eizFBOZrCNTWWC28ok6z0c8HOIyuOuc+jU9O30T5pfJOyxZpj5/QNzZlIMK+5CuVOawaymuIdQHw7dA57eJVDHK7Ys+jDeXGVZ/Q6C/pKxaCWppPE0yt5F7G9ZSqGiazPTWxNbkd5ZAaldO3LDQaViKToSV9nQTKE/pS8ruMkBrC3N5E/pFB+N7wmlgptt25LRFmaMurEcHn8tOq6Cv6FfSxWrw4SA/bWm1FybqpZZO8ri6S120qLI85Q9escraM2ao9Mp7QZ6ZT6yq7VMfsbeW8cRAx9jZBOqsZP5+ac1sKB1vhij67yGNU0GdjMIYEfTCz7Wvy36HrIifHG6RTbbWLXGQGvB79j4K+oqGtwbS010HSJ/fCntDXxEReVTURmhi28bU4qA/kc0zn5VPho2bLk633L9EHc6Wib4Dz20PVPVuWNa75j2r48C3dsu5wboz62/eU9NsGPxvHSX8TTq305Ri1SMpr0iZ2X3gt0C7vqylhvEp/8Q59dTcMUh+PaVtAORn1fpC5FAE0Taf+Bn9Jn13Q4HfyZAafJX7uwGqnzxbSDjKqSJjSfjE+Z0f1gXyCUKQwX6Z/fYv+sPCCFS2SD9unsV6eBywAYX9pnZK+YoClKelMHmvTo+Lht9GXMau3imj61Crlc1cDWw1pRL1K3zq/ZXkqF0T/BLckgouwesab92Z6Kvq1zG2WydPXnmpnJXwt9BnY/g2FNZlL9PepFdM38uyypVt6mf4nONPL9MvTMwYcv301dJghmx77gGO+namirxgRpG+n/l/m52bdbaGvr2WvUfQTHM/KVAKwgVmZYjr/FH3FAhvgk7SRACNx0knNXpMEfT5CW63ENjA9Lu+ihT7YRNrHam+sbyT6WVE4fLqxtJS9TH/QEX14fkf4VdBINmW6u5GgXy9vGDN9UzleLfTBymU/qn5aYHeZBrauYCmb/Ar6wL6HwsMMwCIY9RZskOgbG0Q/0tJd+y030G30ZRCrrcYLmZ9y+8TMGMBBBfmzl+lvuqIPttmeoA+X3VNvwQaJvqKh6OPH+bJSV8vn9OWDq/1yW+ogrwdJ9+GdAhfm5+iDHf9U0IeddXsrIJXpc+xx7YeiiKmZPtPB0dW0EgjhJt3nsHBILlt5lb7eGX1QnCrFtvgW0O9twy/TB45TKu9DFOk307fO4Ki9apSd0ofx4s1voG+AOkqZ/hLQ/5axj+42U1gGaZrpQ//V3mrNSr05mDyWX5L5JfSnP0y/5nGlK2/RsIX+Ghx94pcgX/pX0Oe/ir4SRJh+FY1poQ/3ScMn9OHYH/wGywPt/ker3f8m+tYZeVzCKfqS3b+3+yXI7v/CVbeV/umb6CMnT7qDtj3PGByN2nfG0LN5a8eZBp8kmv6/pw92nF4r/W/Z7yu1sJ93rczzV/b7anxtpw+3sw9plry537/0QB90tj/66bIpv5cAww03sTa20YcJovY3mtGLX7d3Ig0wylYt4P+Cvtw+bKXfW3wf0wer/VQqKfxKnAdCrWRtjTQ4A5wJ5w36KLdVLSEd0geuYZdvSUGl15HtBQiG7aTl5isxzsRjCeo9NWepmYGJAHUqRW1fpu8D+hXlN+nHwsDAu4p6q+fMst/y9k+qI7DlDeSX4vtNg59/xqP0/GYkt7PX/z67ogTALdlX5RfvRdkk+jDKFvWV27IWqQkdSsisa2UhPDm+0ZbbQl97WD3Qlp9fnXxu8SN4TlKI+WX6GqC8K/+hN/o9xfeZnr0mPr1Kg19UI8gx+Na8Ln/Awe/NZOPDgrVTeAHMB4b/jay6VCmXnalcdpkCUgiv0peyKyZwTvqiX9YZuVLVLLuUOK9yVqG1pkFB5ZPTu1FmDVng3z01LDZCcpGJWuUNFFzq9wX6ML5R5V21I9hWfYE+WFzb6feT17Wq5Uv+ME+5jYmBCW+t5+H4zS87fvA8uDYeegmPqLDL1gU2G+XFBYxDg/EF+ooG5lt8NRljhjbzXh37gP5Oog8v3At9Zlabeykjm6wFqPd5W0g/ErXJGqpFSeU5J9fJEYWVJUJVserMNzk3lbXjvGh58DQK59fLeXRSh82VhF+lr7TR7yWrLr+A7n0K029F2S2BLyUFM1he54nCdmY21AQLzUVDH+UP3PtjNoxWHqjpm36Bvr5BhbCOG9tqeHlv7Av6sP6iF/om+CqU9M2R3AGfyPDNJX4FJF6IDPq1Vg8v7nUvJ3CXKIinrtIDB5DyXV2rE0P6rughY/XXAJLtFgP0K2uK6AfVjaGqXquFfh9F5Dp0PqVKb5a+n/8hV04HNWwJfjH6rUtUh5Hf6h68O2fM8Ec51DSFACv94s+q+BiaqoPYTTVkItQbhxWA6qNojyoJT3qJH7mKE638AdJX72bX+FmtZP9YbUKCg63uhGFnMNlQKpRsioYKggt5R/TmkIbf41HtkwFrmBP85WtUONe2Fx9kMvH7WPZdQ/WXyePKbQmir7oLvZG+GpU/BGgp2ysdF5Vo+DVR6aN/qSt/EFPBODaMWDX9ooOwKuZ6UhuNK3dT83yDuQtM9vTuW5iOGp55I31b+hhZsATXCw8arn5NnfVsCNXOvxtbjfST55L/EKDXCNVbVx/iylWrX0gUVwtvcF+J+2T1bG+hj6UULtA3d2DKVreR3xAg4ZeHtME8zRNja9Q+EhTmH/rktRHiigXQHEyq5zjdp9Ol4eWTbNWpn7+IqeM3wyp/t/522an1g3X/Ruy8GWBtxtWvyl7c5nhQb1r8w1nuks6ZP7q5jvcROtFxMDaav+TFDH1xnOySVqfHVc8en187cR73Y4viz0prX5zIMsbLyPE8Z7JMnn126IzPk72zyxajJdR2UJx/jTQvLnzFJ9r4Spdilo4lTS4mOxis1rL8B4SX5e95Zy+BP/uqMCu8sfLDI7W+lCeuXdlqPk/leDe3ZwaHMtrOr3/twiQSiUQikUgkEolEIpFIJBKJRCKRSCQSiUQikUgkEolEIpFIJBKJRCKRSCQS6f9K/wPh9PMZzOekFQAAAABJRU5ErkJggg==" />
        </div>

        <a class="nav-link signin" href="#">
          Sign In
        </a>

        <button class="btn btn-success">Get Started</button>
        {/* <div className="profile">
          <div className="profile-h">
            <img src="http://s3.amazonaws.com/37assets/svn/765-default-avatar.png" />
            <div>Nosenti</div>
          </div>
        </div> */}
      </div>
    );
  }
}

export default Header;
