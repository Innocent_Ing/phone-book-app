
import React from 'react';
import Action from './Action.js';
import { withRouter } from "react-router-dom";
class ViewContact extends React.Component {

    apiAddress = "https://code-catalist-phone-book-rails.herokuapp.com";

    state = {
        contact: undefined
    };

    goBack = () => {
        this.props.history.push('');
    }
    componentWillMount = () => {
        fetch(`${this.apiAddress}/contacts/${this.props.match.params.id}`, {
            method: "get",
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json'
            },
        }).then(resp => {
            return resp.json()
        })
            .then(resp => {
                console.log(resp);
                this.setState({contact : resp});
            }).catch(err => {
                console.log("Error editing the contact")
            })

    }

    render() {
        return (
            <div className="ViewContact">
                {/*  */}
                <div>
                    <button onClick={this.goBack} className="goBack">
                        <span className="mdi mdi-arrow-left"></span>
                    </button>
                </div>
                <div>
                    {this.state.contact === undefined ?
                        null :
                        <div>
                            {/*  */}
                            <div className="view-aside">
                                <div>
                                    <img src="https://via.placeholder.com/140x100" />
                                </div>
                                <div>
                                    <div style={{ marginLeft: "15px" }}>
                                        <h1>{this.state.contact.name}</h1>
                                    </div>
                                    <div className="btns-h">
                                        <button><span className="mdi mdi-message"></span></button>
                                        <button><span className="mdi mdi-phone"></span></button>
                                        <button><span className="mdi mdi-video"></span></button>
                                        <button><span className="mdi mdi-email"></span></button>
                                    </div>
                                </div>
                            </div>
                            {/*  */}
                            <div>
                                <div className="content">
                                    <span className="mdi mdi-phone"></span>
                                    <div className="content-v">
                                        <span>{this.state.contact.phone_number}</span>
                                        <span>Mobile</span>
                                    </div>
                                </div>
                                <div className="content">
                                    <span className="mdi mdi-home"></span>
                                    <div className="content-v">
                                        <span>0930928340-342</span>
                                        <span>Home</span>
                                    </div>
                                </div>
                                <div className="content">
                                    <span className="mdi mdi-email"></span>
                                    <div className="content-v">
                                        <span>innocent@kjs.com</span>
                                        <span>Email</span>
                                    </div>
                                </div>
                            </div>
                            {/*  */}

                        </div>}
                </div>
            </div>
        )
    }
}

export default withRouter(ViewContact);