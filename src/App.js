import React from "react";
import createHistory from "history";
import "./App.css";
import Header from "./components/common/Header.js";
import Action from "./components/Action.js";
import Phonebook from "./components/Phonebook.js";
import Footer from "./components/common/Footer.js";
import "./styles/style.css";
import "./../node_modules/bootstrap/dist/css/bootstrap.min.css";
import Register from "./components/Register";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import ViewContact from "./components/ViewContact";
import HomePage from "./components/homepages/HomePage";

function App() {
  return (
    <div>
      <Header />
      {/* The Router */}
      <Router>
        {/*  */}
        <Switch>
          <Route path="/edit/:id">
            <Register isEdit={true} />
          </Route>
          <Route path="/create" component={Register} />
          <Route path="/view/:id" component={ViewContact} />
          <Route exact path="/" component={HomePage} />
        </Switch>
        {/*  */}
      </Router>
      {/* Footer  */}
      <Footer />
    </div>
  );
}

export default App;
